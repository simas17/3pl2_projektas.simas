﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal;
using Models = Dal.Models;
using FormModels = Dal.FormModels;

namespace Services
{
    public class BooksService : IDisposable
    {
        private readonly LibraryDbContext _db;

        public BooksService()
        {
            _db = new LibraryDbContext();
        }

        public List<FormModels.BooksSearchResult> BookSearch()
        {
            var data = (from book in _db.Books
                        let categories = _db.Categories.Join(_db.BooksByCategories,
                                                category => category.Id,
                                                bookByCategory => bookByCategory.CategoryId,
                                                (category, bookByCategory) => new { Categories = category, BooksByCategory = bookByCategory })
                                .Where(m => m.BooksByCategory.BookId == book.Id)
                                .Select(m => m.Categories.Name)
                                .ToList()
                        let authors = _db.Authors.Join(_db.BooksByAuthors,
                                               author => author.Id,
                                               bookByAuthor => bookByAuthor.AuthorId,
                                               (author, bookByAuthor) => new { Authors = author, BooksByAuthors = bookByAuthor })
                                .Where(m => m.BooksByAuthors.BookId == book.Id)
                                .Select(m => m.Authors.Firstname + " " + m.Authors.Lastname)
                                .ToList()
                        let booksAtLibraries = _db.Libraries.Join(_db.BooksAtLibraries,
                                                    library => library.Id,
                                                    bookAtLibrary => bookAtLibrary.LibraryId,
                                                    (library, bookAtLibrary) => new {
                                                        Library = library,
                                                        BookAtLibrary = bookAtLibrary
                                                    })
                                               .Where(m => m.BookAtLibrary.BookId == book.Id)
                                               .Select(m => m.BookAtLibrary.Quantity)
                                               .ToList()
                        select new
                        {
                            BookId = book.Id,
                            Isbn = book.Isbn,
                            Title = book.Title,
                            DateOfPublish = book.DateOfPublish,
                            Categories = categories,
                            Authors = authors,
                            BooksAtLibraries = booksAtLibraries
                        }
                        )
                        .AsEnumerable()
                        .Select(m => new FormModels.BooksSearchResult
                        {
                            BookId = m.BookId,
                            Isbn = m.Isbn,
                            Title = m.Title,
                            DateOfPublish = m.DateOfPublish,
                            Categories = string.Join(", ", m.Categories),
                            Authors = string.Join(", ", m.Authors),
                            Quantity = m.BooksAtLibraries.Sum()
                        }).ToList();


            return new List<FormModels.BooksSearchResult>();
        }

        public void BookAdd(FormModels.BookAdd bookAdd)
        {
            using(var transaction = _db.Database.BeginTransaction())
            {
                try
                {
                    var book = new Models.Book(null, bookAdd.Title, bookAdd.DateOfPublish);
                    _db.Books.Add(book);
                    _db.SaveChanges();

                    foreach (var category in bookAdd.CategoryIds)
                    {
                        var bookByCategory = new Models.BookByCategory(book.Id, category);
                        _db.BooksByCategories.Add(bookByCategory);
                    }

                    foreach (var author in bookAdd.AuthorIds)
                    {
                        var bookByAuhor = new Models.BookByAuthor(book.Id, author);
                        _db.BooksByAuthors.Add(bookByAuhor);
                    }

                    _db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
