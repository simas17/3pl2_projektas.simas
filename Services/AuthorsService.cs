﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FormModels = Dal.FormModels;
using Models = Dal.Models;
using Dal;

namespace Services
{
    public class AuthorsService : IDisposable
    {
        private readonly LibraryDbContext _db;

        public AuthorsService()
        {
            _db = new LibraryDbContext();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<FormModels.Author> GetAuthors()
        {
            return _db.Authors.Select(m => new FormModels.Author()
            {
                Id = m.Id,
                Firstname = m.Firstname,
                Lastname = m.Lastname
            }).ToList();
        }
    }
}
