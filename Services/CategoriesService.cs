﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal;
using Dal.Models;
using FormModels = Dal.FormModels;
using MySql.Data.MySqlClient;

namespace Services
{
    public class CategoriesService : BaseService, IDisposable
    {
        private readonly LibraryDbContext _db;
        public CategoriesService()
        {
            _db = new LibraryDbContext();
        }

        public List<Category> GetCategories()
        {
            //1 MysqlCommand kurimas
            using (var connection = GetConnetion())
            {
                var cmdText = "Get_Categories";
                MySqlCommand getCategoriesCommand = new MySqlCommand(cmdText, connection);
                // 1.2 KomandosTipas
                getCategoriesCommand.CommandType = System.Data.CommandType.StoredProcedure;
                // 1.3 Parametrai

                //2 Duomenų gavimas ir apdorojimas
                MySqlDataAdapter adapter = new MySqlDataAdapter(getCategoriesCommand);
                DataTable table = new DataTable();
                adapter.Fill(table);

                var categories = table.AsEnumerable().Select(m => new Category(
                        m.Field<int?>("Id"),
                        m.Field<string>("Name")
                    )).ToList();

                //3 Duomenų grąžinimas
                return categories;
            }
        }

        public void Save(Category category)
        {
            if (category.Id.HasValue)
            {
                try
                {
                    using (var connection = GetConnetion())
                    {
                        var cmdText = "Update_Category";
                        MySqlCommand updateCategoryCommand = new MySqlCommand(cmdText, connection);
                        updateCategoryCommand.CommandType = CommandType.StoredProcedure;
                        updateCategoryCommand.Parameters.AddWithValue("InName", category.Name);
                        updateCategoryCommand.Parameters.AddWithValue("InId", category.Id);
                        connection.Open();

                        updateCategoryCommand.ExecuteNonQuery();

                        connection.Close();

                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            } else
            {
                try
                {
                    using (var connection = GetConnetion())
                    {
                        var cmdText = "Add_Category";
                        MySqlCommand addCategoryCommand = new MySqlCommand(cmdText, connection);
                        addCategoryCommand.CommandType = CommandType.StoredProcedure;
                        addCategoryCommand.Parameters.AddWithValue("InName", category.Name);
                        connection.Open();

                        addCategoryCommand.ExecuteNonQuery();

                        connection.Close();

                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void DeleteCategory(int categoryId)
        {
            using (var connection = GetConnetion())
            {
                var deleteCategoryCmdText = "Delete_category";
                var deleteCategoryCmd = new MySqlCommand(deleteCategoryCmdText, connection);
                deleteCategoryCmd.CommandType = CommandType.StoredProcedure;
                deleteCategoryCmd.Parameters.AddWithValue("InCategoryId", categoryId);

                connection.Open();

                deleteCategoryCmd.ExecuteNonQuery();

                connection.Close();

            }
        }

        public List<CategoryModel> GetCategoriesNew()
        {
            return _db.Categories.ToList();
        }

        public void SaveCategory(FormModels.Category category)
        {
            if (category == null) return;

            var categoryFromDb = _db
                                    .Categories
                                    .Where(m => m.Id == category.Id)
                                    .FirstOrDefault();

            if (categoryFromDb == null) {
                var categoryModel = new CategoryModel(category.Name);
                _db.Categories.Add(categoryModel);
                //insert
            } else {
                //update
                categoryFromDb.Update(category.Name);
            }

            _db.SaveChanges();


        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
