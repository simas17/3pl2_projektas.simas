﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.FormModels
{
    public class BooksSearchResult
    {
        public int BookId { get; set; }

        public string Isbn { get; set; }

        public string Title { get; set; }

        public DateTime? DateOfPublish { get; set; }

        public string Categories { get; set; }

        public string Authors { get; set; }

        public int Quantity { get; set; }
    }
}
