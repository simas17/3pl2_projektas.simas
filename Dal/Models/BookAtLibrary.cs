﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("booksatlibraries")]
    public class BookAtLibrary
    {
        [Key]
        [Column(Order = 1)]
        public int BookId { get; private set; }

        [Key]
        [Column(Order = 2)]
        public int LibraryId { get; private set; }

        public int Quantity { get; private set; }

        public BookAtLibrary() { }

        public BookAtLibrary(int bookId, int libraryId, int quantity)
        {
            BookId = bookId;
            LibraryId = libraryId;
            Quantity = quantity;
        }
    }
}
